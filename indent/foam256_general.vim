"----------------------------------------------"
" Contributor: Tobias Holzmann                 "
" Last Change: May 2015                        "
" Version:     4.0                             "
" Email:       Tobias.Holzmann@Holzmann-cfd.de "
"----------------------------------------------"


"" indentation general indentation 
" -----------------------------------------------------------------------------
runtime indent/foam256_indentation.vim
