"----------------------------------------------"
" Contributor: Tobias Holzmann                 "
" Last Change: May 2015                        "
" Version:     4.0                             "
" Email:       Tobias.Holzmann@Holzmann-cfd.de "
"----------------------------------------------"


" regionProperties
"-------------------------------------------------------------------------------

    syn match regionPropertiesDict
    \ "fluid\{1} \{1}
    \ \|solid\{1} \{1}"
    hi link regionPropertiesDict foam256_keyOnLeftSide


"-------------------------------------------------------------------------------
