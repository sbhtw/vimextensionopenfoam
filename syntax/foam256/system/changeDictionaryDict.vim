"----------------------------------------------"
" Contributor: Tobias Holzmann                 "
" Last Change: May 2015                        "
" Version:     4.0                             "
" Email:       Tobias.Holzmann@Holzmann-cfd.de "
"----------------------------------------------"


" changeDictionaryDict
"-------------------------------------------------------------------------------

    "- Dictionarys in the file 
    syntax keyword dictReplace 
    \ dictionaryReplacement
    highlight link dictReplace foam256_dictionary


"-------------------------------------------------------------------------------
